# Elefante Custom Widget

A Flutter package for both android and iOS which provides Custom Widget

## Screenshots

<img src="./screenshot/screenshoot.png" height="400em" width="225em" />

## Set-up

- [x] add description in pubspec
- [x] add repository in pubspec
- [x] change default README
- [x] add LICENSE
- [x] protected tag

## Usage

To use this package :

- add the dependency to your pubspec.yaml file.

```yaml
dependencies:
  flutter:
    sdk: flutter
  infra_custom_widget:
```

### List Of Widget

- Appbars
- Buttons
- Dialogs
- Loading
- PIN
- QR
- Dropdowns
- Selections
- Separators
- Shimmer
- Table Scroll
- TextFields
- Texts
- Timelines
