import 'package:cached_network_image/cached_network_image.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:infra_custom_widget/core/utils/controllers/search_dropdown_controller.dart';
import 'package:infra_custom_widget/data/helper/dropdown_model.dart';
import 'package:infra_custom_widget/widgets/texts.dart';
import 'package:loading_animations/loading_animations.dart';

///All textFields used in the App

class SearchDropdown {
  static Widget defaultSearchDropdown({
    Key? key,
    required SearchDropdownController searchDropdownController,
  }) {
    return Material(
      color: Colors.transparent,
      borderRadius: BorderRadius.circular(10.r),
      child: AnimatedSize(
        duration: const Duration(milliseconds: 300),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 5.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DropdownSearch<DropdownModel>(
                dropdownBuilder: customDropdownSearchBuilder,
                items: searchDropdownController.items,
                itemAsString: (DropdownModel? u) => u?.text ?? "",
                compareFn: (item, selectedItem) => item.id == selectedItem.id,
                onChanged: searchDropdownController.onChange,
                selectedItem: searchDropdownController.selectedItem,
                popupProps: PopupProps.dialog(
                  dialogProps: DialogProps(
                    backgroundColor: Get.theme.scaffoldBackgroundColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.r),
                    ),
                  ),
                  showSearchBox: true,
                  showSelectedItems: true,
                  itemBuilder: customPopupSearchItemBuilder,
                  title: Center(
                    child: Padding(
                      padding: EdgeInsets.all(10.0.spMin),
                      child: Texts.headline6(
                        searchDropdownController.label,
                      ),
                    ),
                  ),
                ),
                dropdownDecoratorProps: DropDownDecoratorProps(
                  dropdownSearchDecoration: InputDecoration(
                    hintText: 'Pilih ${searchDropdownController.label}',
                    enabledBorder: OutlineInputBorder(
                      // width: 0.0 produces a thin "hairline" border
                      borderSide: BorderSide(
                        color: !searchDropdownController.isValid
                            ? Get.theme.colorScheme.error
                            : Get.theme.shadowColor,
                        width: 1.w,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.r),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      // width: 0.0 produces a thin "hairline" border
                      borderSide: BorderSide(
                          color: Get.theme.colorScheme.error, width: 1.w),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.r),
                      ),
                    ),
                    border: const OutlineInputBorder(),
                    // label: Texts.body2(searchDropdownController.label),
                    contentPadding: EdgeInsets.only(
                      top: 1.h,
                      bottom: 1.h,
                      left: 20.w,
                    ),
                    filled: true,
                    fillColor: Get.theme.colorScheme.background,
                    hintStyle: TextStyle(
                      color: Get.theme.textTheme.bodyLarge?.color,
                    ),
                  ),
                ),
              ),
              AnimatedSize(
                duration: const Duration(milliseconds: 300),
                child: !searchDropdownController.isValid
                    ? Padding(
                        padding: EdgeInsets.all(8.0.spMin),
                        child: Texts.caption(
                          "Wajib disertakan",
                          color: Get.theme.colorScheme.error,
                        ),
                      )
                    : const SizedBox(
                        height: 10,
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  static Widget imageSearchDropdown({
    Key? key,
    required SearchDropdownController searchDropdownController,
    EdgeInsetsGeometry padding = const EdgeInsets.symmetric(vertical: 5),
    Mode mode = Mode.BOTTOM_SHEET,
  }) {
    return AnimatedSize(
      duration: const Duration(milliseconds: 300),
      child: Padding(
        padding: padding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            DropdownSearch<DropdownModel>(
              compareFn: (item, selectedItem) => item.id == selectedItem.id,
              items: searchDropdownController.items,
              onChanged: searchDropdownController.onChange,
              selectedItem: searchDropdownController.selectedItem,
              dropdownBuilder: (context, selectedItem) => customDropDownImage(
                  context, selectedItem,
                  hint: 'Pilih ${searchDropdownController.label}'),
              popupProps: PopupProps.dialog(
                dialogProps: DialogProps(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.r),
                      topRight: Radius.circular(10.r),
                    ),
                  ),
                ),
                showSelectedItems: true,
                title: Center(
                  child: Padding(
                    padding: EdgeInsets.all(15.0.spMin),
                    child: Texts.headline6(searchDropdownController.label),
                  ),
                ),
                itemBuilder: customPopupItemBuilderImage,
              ),
              dropdownDecoratorProps: DropDownDecoratorProps(
                dropdownSearchDecoration: InputDecoration(
                  hintText: 'Pilih ${searchDropdownController.label}',
                  enabledBorder: OutlineInputBorder(
                    // width: 0.w.0 produces a thin "hairline" border
                    borderSide: BorderSide(
                      color: !searchDropdownController.isValid
                          ? Get.theme.colorScheme.error
                          : Get.theme.shadowColor,
                      width: 1.w,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.r),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    // width: 0.w.0 produces a thin "hairline" border
                    borderSide: BorderSide(
                      color: Get.theme.primaryColor,
                      width: 1.w,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.r),
                    ),
                  ),
                  border: const OutlineInputBorder(),
                  contentPadding: const EdgeInsets.only(
                    top: 5,
                    bottom: 5,
                    left: 25,
                  ),
                  filled: true,
                  fillColor: Get.theme.colorScheme.background,
                ),
              ),
            ),
            AnimatedSize(
              duration: const Duration(milliseconds: 300),
              child: !searchDropdownController.isValid
                  ? Padding(
                      padding: EdgeInsets.all(8.0.spMin),
                      child: Texts.caption(
                        "Wajib disertakan",
                        color: Get.theme.colorScheme.error,
                      ),
                    )
                  : const SizedBox(),
            ),
          ],
        ),
      ),
    );
  }

  static Widget customDropDownImage(BuildContext context, DropdownModel? item,
      {String? hint}) {
    if (item == null) {
      return ListTile(
        tileColor: Colors.transparent,
        contentPadding: EdgeInsets.zero,
        visualDensity: VisualDensity.compact,
        title: Texts.body2('$hint', color: Get.theme.hintColor),
      );
    }

    return ListTile(
      tileColor: Colors.transparent,
      visualDensity: VisualDensity.compact,
      contentPadding: EdgeInsets.zero,
      leading: item.urlImage == null
          ? null
          : CachedNetworkImage(
              width: 80.w,
              height: 50,
              imageUrl: item.urlImage ?? "",
              placeholder: (context, url) => LoadingBouncingLine.circle(
                backgroundColor: Get.theme.primaryColor,
              ),
              errorWidget: (context, url, error) => const Icon(
                Icons.error,
              ),
            ),
      title: Texts.body2(
        item.name,
        textOverflow: TextOverflow.visible,
      ),
      subtitle: item.subtitle == null
          ? null
          : Texts.overline(
              item.subtitle ?? "-",
              textOverflow: TextOverflow.visible,
            ),
    );
  }

  static Widget customPopupItemBuilderImage(
      BuildContext context, DropdownModel? item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 5.h),
      decoration: BoxDecoration(
        border: Border.all(
          color: isSelected
              ? Theme.of(context).primaryColor
              : Get.theme.shadowColor,
        ),
        borderRadius: BorderRadius.circular(10.r),
        color: Get.theme.colorScheme.background,
      ),
      child: ListTile(
        tileColor: Colors.transparent,
        visualDensity: VisualDensity.compact,
        contentPadding: item?.subtitle != null
            ? null
            : EdgeInsets.symmetric(horizontal: 16, vertical: 5.h),
        leading: item?.urlImage == null
            ? null
            : CachedNetworkImage(
                width: 80.w,
                height: 50,
                imageUrl: item?.urlImage ?? "",
                placeholder: (context, url) => LoadingBouncingLine.circle(
                  backgroundColor: Get.theme.primaryColor,
                ),
                // errorWidget: (context, url, error) =>  Icon(Icons.error),
              ),
        title: Texts.caption(
          item?.name ?? "-",
          textOverflow: TextOverflow.visible,
        ),
        subtitle: item?.subtitle == null
            ? null
            : Texts.overline(
                item?.subtitle ?? "-",
                textOverflow: TextOverflow.visible,
              ),
        trailing: isSelected
            ? Icon(
                Icons.check_circle,
                color: Get.theme.primaryColor,
              )
            : Icon(
                Icons.circle_outlined,
                color: Get.theme.shadowColor,
              ),
      ),
    );
  }

  static Widget cupertinoPicker(BuildContext context) {
    return AnimatedSize(
      duration: const Duration(milliseconds: 300),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 5.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CupertinoTextField(
              // placeholder: ,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10.h),
              onTap: () {
                // showCupertinoModalPopup(context: context, builder: (BuildContext context){

                // });
              },
            ),
          ],
        ),
      ),
    );
  }

  static Widget customPopupSearchItemBuilder(
      BuildContext context, DropdownModel? item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 5.h),
      decoration: BoxDecoration(
        border: Border.all(
          color: isSelected
              ? Theme.of(context).primaryColor
              : Get.theme.shadowColor,
        ),
        borderRadius: BorderRadius.circular(10.r),
        color: Get.theme.colorScheme.background,
      ),
      child: ListTile(
        tileColor: Colors.transparent,
        visualDensity: VisualDensity.compact,
        contentPadding: item?.subtitle != null
            ? null
            : EdgeInsets.symmetric(horizontal: 16, vertical: 5.h),
        leading: item?.urlImage == null
            ? null
            : CachedNetworkImage(
                width: 80.w,
                height: 50,
                imageUrl: item?.urlImage ?? "",
                placeholder: (context, url) => LoadingBouncingLine.circle(
                  backgroundColor: Get.theme.primaryColor,
                ),
                // errorWidget: (context, url, error) =>  Icon(Icons.error),
              ),
        title: Texts.body2(
          item?.name ?? "-",
          textOverflow: TextOverflow.visible,
        ),
        subtitle: item?.subtitle == null
            ? null
            : Texts.caption(
                item?.subtitle ?? "-",
                color: Get.theme.shadowColor,
                textOverflow: TextOverflow.visible,
              ),
        // trailing: isSelected
        //     ? Icon(
        //         Icons.check_circle,
        //         color: Get.theme.primaryColor,
        //       )
        //     : Icon(
        //         Icons.circle_outlined,
        //         color: Get.theme.shadowColor,
        //       ),
      ),
    );
  }

  static Widget customDropdownSearchBuilder(
      BuildContext context, DropdownModel? item) {
    return ListTile(
      tileColor: Colors.transparent,
      visualDensity: VisualDensity.compact,
      contentPadding: EdgeInsets.zero,
      leading: item?.urlImage == null
          ? null
          : CachedNetworkImage(
              width: 80.w,
              height: 50,
              imageUrl: item?.urlImage ?? "",
              placeholder: (context, url) => LoadingBouncingLine.circle(
                backgroundColor: Get.theme.primaryColor,
              ),
              // errorWidget: (context, url, error) =>  Icon(Icons.error),
            ),
      title: Texts.body2(
        item?.name ?? "-",
      ),
      subtitle: item?.subtitle == null
          ? null
          : Texts.caption(
              item?.subtitle ?? "-",
              color: Get.theme.shadowColor,
            ),
    );
  }
}
