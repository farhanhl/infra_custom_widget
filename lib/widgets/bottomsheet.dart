import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:infra_custom_widget/widgets/cards.dart';

import 'package:infra_custom_widget/widgets/texts.dart';
import 'package:infra_custom_widget/widgets/buttons.dart';

import '../core/utils/asset_helper.dart';

class BottomSheets {
  static Widget cameraOption() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(
          top: 10.h,
          bottom: 20.h,
          left: 20.w,
          right: 20.w,
        ),
        decoration: BoxDecoration(
          color: Get.theme.scaffoldBackgroundColor,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.r), topRight: Radius.circular(15.r)),
          // boxShadow: [
          //   BoxShadow(
          //       blurRadius: 30, offset: const Offset(0, 5), color: Colors.black)
          // ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Padding(
                padding: EdgeInsets.all(8.0.sp),
                child: Texts.headline6("Pilih Gambar"),
              ),
            ),
            const Divider(),
            Padding(
              padding: EdgeInsets.only(top: 20.h, bottom: 10.h),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Cards.bottomSheetMenu(
                    IconCardItem(
                      iconData: Icons.camera_alt,
                      imageAsset: AssetHelper.camera,
                      label: "Kamera",
                    ),
                    () {
                      Get.back(result: ImageSource.camera);
                    },
                  ),
                  Cards.bottomSheetMenu(
                    IconCardItem(
                      iconData: Icons.photo,
                      imageAsset: AssetHelper.gallery,
                      label: "Galeri",
                    ),
                    () {
                      Get.back(result: ImageSource.gallery);
                    },
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  static Widget filter({
    List<String>? limitItems,
    List<String>? categoryItems,
    String? selectedCategoryItem,
    String? selectedLimitItem,
    Function(String?)? limitFunction,
    Function(String?)? categoryFunction,
  }) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.only(
              top: 10.h,
              bottom: 20.h,
              left: 20.w,
              right: 20.w,
            ),
            decoration: BoxDecoration(
              color: Get.theme.colorScheme.background,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15.r),
                  topRight: Radius.circular(15.r)),
              boxShadow: [
                BoxShadow(
                  blurRadius: 30,
                  offset: const Offset(0, 5),
                  color: Get.theme.shadowColor,
                )
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Padding(
                    padding: EdgeInsets.all(8.0.sp),
                    child: Texts.headline6("Filter"),
                  ),
                ),
                const Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Texts.subtitle2('Limit'),
                        SizedBox(
                          width: Get.width.w / 3,
                          child: DropdownButtonFormField<String>(
                            // decoration: InputDecoration(
                            //   enabledBorder: OutlineInputBorder(
                            //     borderRadius: BorderRadius.circular(10.r),
                            //   ),
                            // ),
                            value: selectedLimitItem,
                            items: limitItems!
                                .map((item) => DropdownMenuItem<String>(
                                      value: item,
                                      child: Texts.caption(item),
                                    ))
                                .toList(),
                            onChanged: limitFunction,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Texts.subtitle2('Category'),
                        SizedBox(
                          width: Get.width.w / 3,
                          child: DropdownButtonFormField<String>(
                            // decoration: InputDecoration(
                            //   enabledBorder: OutlineInputBorder(
                            //     borderRadius: BorderRadius.circular(10.r),
                            //   ),
                            // ),
                            value: selectedCategoryItem,
                            items: categoryItems!
                                .map((item) => DropdownMenuItem<String>(
                                      value: item,
                                      child: Texts.caption(item),
                                    ))
                                .toList(),
                            onChanged: categoryFunction,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  width: double.infinity,
                  child: Buttons.defaultButton(
                    handler: Get.back,
                    widget: Texts.button('Terapkan'),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
