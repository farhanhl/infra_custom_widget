import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loading_animations/loading_animations.dart';

class LoadingActivity {
  static Widget platformLoading({
    AnimationController? controller,
    Color? borderColor,
    required Color? color,
    bool animating = true,
    required double size,
    double? borderSize,
    Widget? Function(BuildContext, int)? itemBuilder,
    Duration? duration = const Duration(milliseconds: 3000),
  }) {
    if (Platform.isIOS) {
      return CupertinoActivityIndicator(
        color: color,
        animating: animating,
        radius: size / 3.r,
      );
    } else {
      return LoadingBouncingLine.circle(
        backgroundColor: color!,
        size: size,
      );
    }
  }
}
