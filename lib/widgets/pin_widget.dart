import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class PinWidget {
  static Widget defaultPin({
    required Function(String) onChanged,
    Function(String)? onCompleted,
    required int length,
    TextInputType keyboardType = TextInputType.number,
    bool obscureText = true,
    BorderRadius? borderRadius,
    Color? inactiveColor,
    Color? activeColor,
    MainAxisAlignment mainAxisAlignment = MainAxisAlignment.spaceBetween,
    PinCodeFieldShape pinCodeFieldShape = PinCodeFieldShape.box,
  }) {
    return PinCodeTextField(
      mainAxisAlignment: mainAxisAlignment,
      appContext: Get.context!,
      length: length,
      onChanged: onChanged,
      onCompleted: onCompleted,
      keyboardType: keyboardType,
      obscureText: obscureText,
      pinTheme: PinTheme(
        shape: pinCodeFieldShape,
        borderRadius: borderRadius,
        inactiveColor: inactiveColor,
        activeColor: activeColor,
      ),
    );
  }
}
