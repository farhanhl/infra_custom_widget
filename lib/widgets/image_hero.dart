import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'image_viewer.dart';
import './texts.dart';

class HeroImage extends StatelessWidget {
  final String imgPath;
  final double? height;
  final String id;
  final String? label;
  final BoxFit? fit;
  final bool useBorder;
  final double? width;
  final bool isRounded;
  final Color? color;
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  const HeroImage({
    Key? key,
    this.height,
    required this.imgPath,
    this.id = "",
    this.label,
    this.fit,
    this.useBorder = false,
    this.width = double.infinity,
    this.isRounded = false,
    required this.color,
    this.padding = EdgeInsets.zero,
    this.margin,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding!,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
            onTap: () {
              Get.to(() => HeroPhotoViewRouteWrapper(
                    imageProvider: CachedNetworkImageProvider(imgPath),
                    tag: id,
                  ));
            },
            child: Hero(
              tag: id,
              child: Container(
                margin: margin,
                decoration: BoxDecoration(
                  color: color,
                  borderRadius: BorderRadius.all(Radius.circular(10.r)),
                  border: useBorder
                      ? Border.all(
                          width: 1,
                          color: Colors.grey,
                        )
                      : null,
                ),
                width: width,
                height: height,
                child: ClipRRect(
                  borderRadius: isRounded
                      ? BorderRadius.circular(1000.r)
                      : BorderRadius.circular(14.0.r),
                  child: CachedNetworkImage(
                    errorWidget: (context, url, error) => const Icon(
                      Icons.broken_image,
                      color: Colors.grey,
                    ),
                    placeholder: (context, url) => const Icon(
                      Icons.broken_image,
                      color: Colors.grey,
                    ),
                    imageUrl: imgPath,
                    fit: fit,
                  ),
                ),
              ),
            ),
          ),
          label == null
              ? const SizedBox()
              : Texts.overline(
                  label ?? "-",
                  textAlign: TextAlign.center,
                ),
        ],
      ),
    );
  }
}
