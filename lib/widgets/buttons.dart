import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:badges/badges.dart' as badges;
import './texts.dart';

class Buttons {
  static Widget defaultButton({
    required VoidCallback handler,
    required Widget widget,
    Color? fillColor,
    bool filled = true,
    EdgeInsets padding = const EdgeInsets.only(top: 10),
    double borderRadius = 5,
  }) {
    return Padding(
      padding: padding,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: filled
              ? null
              : MaterialStateProperty.all<Color?>(Colors.transparent),
          elevation: filled ? null : MaterialStateProperty.all<double?>(0),
          shape: filled
              ? MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(borderRadius),
                  ),
                )
              : null,
        ),
        onPressed: handler,
        child: widget,
      ),
    );
  }

  static Widget textButton({
    required VoidCallback handler,
    required Widget widget,
    EdgeInsets padding = const EdgeInsets.only(top: 10),
    Color? overlayColor,
  }) {
    return Padding(
      padding: padding,
      child: TextButton(
        onPressed: handler,
        style: ButtonStyle(
          overlayColor: MaterialStateProperty.all<Color?>(overlayColor),
        ),
        child: widget,
      ),
    );
  }

  static Widget buttonCard({
    required String title,
    String? subtitle,
    required IconData iconData,
    required VoidCallback handler,
    bool isTrailing = false,
    bool underlined = true,
    Color? textColor,
    required Color iconColor,
    double? iconSize,
    double? arrowSize,
    bool showBadge = false,
  }) {
    return Column(
      children: [
        InkWell(
          onTap: handler,
          child: Container(
            margin: EdgeInsets.all(10.spMin),
            padding: EdgeInsets.all(10.spMin),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Icon(
                      iconData,
                      color: iconColor,
                      size: iconSize,
                    ),
                    SizedBox(
                      width: 20.w,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Texts.body1(
                          title,
                          color: textColor,
                        ),
                        SizedBox(
                          height: 5.h,
                        ),
                        subtitle != null
                            ? Texts.body2(
                                subtitle,
                                color: Colors.grey,
                              )
                            : const SizedBox(),
                      ],
                    ),
                  ],
                ),
                isTrailing
                    ? badges.Badge(
                        showBadge: showBadge,
                        child: Icon(
                          Icons.chevron_right,
                          size: arrowSize,
                        ),
                      )
                    : const SizedBox(),
              ],
            ),
          ),
        ),
        underlined
            ? Divider(
                height: 0.h,
                thickness: .5,
              )
            : const SizedBox(),
      ],
    );
  }

  static Widget buttonCard2({
    required String title,
    String? subtitle,
    required IconData iconData,
    required VoidCallback handler,
    bool isTrailing = false,
  }) {
    return Column(
      children: [
        InkWell(
          onTap: handler,
          child: Container(
            margin: EdgeInsets.only(left: 10.w, right: 10.w),
            padding: EdgeInsets.only(top: 10.h, left: 10.w, right: 10.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.all(10.spMin),
                  decoration: BoxDecoration(
                    color: Get.theme.primaryColor.withAlpha(50),
                    borderRadius: BorderRadius.circular(15.r),
                  ),
                  child: Icon(
                    iconData,
                    color: Get.theme.primaryColor,
                    size: 35.spMin,
                  ),
                ),
                Expanded(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 10.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Texts.body1(title),
                                SizedBox(
                                  height: 5.h,
                                ),
                                subtitle != null
                                    ? Texts.body2(
                                        subtitle,
                                        color: Colors.grey,
                                      )
                                    : const SizedBox(),
                              ],
                            ),
                            isTrailing
                                ? const Icon(
                                    Icons.chevron_right,
                                  )
                                : const SizedBox(),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                      Divider(
                        height: 0.h,
                        thickness: .3.h,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
