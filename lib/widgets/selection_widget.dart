import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../core/utils/controllers/selection_controller.dart';
import '../data/helper/dropdown_model.dart';
import './texts.dart';
import './loading.dart';

class SelectionWidget extends StatelessWidget {
  final SelectionController selectionController;
  final bool useIcon;
  const SelectionWidget({
    Key? key,
    required this.selectionController,
    this.useIcon = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ...List.generate(
          selectionController.totalRow,
          (rowIndex) {
            List<Widget> childrenRow =
                List.generate(selectionController.column, (columnIndex) {
              DropdownModel? item = selectionController
                  .items[rowIndex * selectionController.column + columnIndex];
              bool isSelected = item.id == selectionController.selectedItem?.id;
              return Expanded(
                child: GestureDetector(
                  onTap: () {
                    selectionController.onChange(item);
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: 5.h,
                      bottom: 5.h,
                      left: (columnIndex == 0 ? 0.w : 5.w),
                      right: (columnIndex == selectionController.column - 1
                          ? 0.w
                          : 5.w),
                    ),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: isSelected
                            ? Get.theme.primaryColor
                            : Platform.isIOS
                                ? Get.theme.shadowColor
                                : Get.theme.colorScheme.background,
                      ),
                      borderRadius: BorderRadius.circular(15.r),
                      color: Get.theme.colorScheme.background,
                    ),
                    child: useIcon
                        ? Padding(
                            padding: EdgeInsets.symmetric(vertical: 20.h),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(
                                  item.iconData,
                                  color: isSelected
                                      ? Get.theme.primaryColor
                                      : Get.theme.shadowColor,
                                  size: 60.spMin,
                                ),
                                Padding(
                                  padding: EdgeInsets.all(10.spMin),
                                  child: Texts.body1(
                                    item.name,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                    vertical: 3.h,
                                    horizontal: 10.w,
                                  ),
                                  child: Texts.overline(
                                    item.subtitle ?? "",
                                    color: isSelected
                                        ? Get.theme.primaryColor
                                        : Get.theme.shadowColor,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ),
                          )
                        : ListTile(
                            leading: isSelected
                                ? Icon(
                                    Icons.check_circle,
                                    color: Get.theme.primaryColor,
                                  )
                                : Icon(
                                    Icons.circle_outlined,
                                    color: Get.theme.shadowColor,
                                  ),
                            visualDensity: VisualDensity.comfortable,
                            trailing: item.urlImage == null
                                ? null
                                : CachedNetworkImage(
                                    width: 80.w,
                                    height: 50.h,
                                    imageUrl: item.urlImage ?? "",
                                    placeholder: (context, url) =>
                                        LoadingActivity.platformLoading(
                                      size: 40.spMin,
                                      color: Get.theme.primaryColor,
                                    ),
                                    errorWidget: (context, url, error) =>
                                        const Icon(
                                      Icons.error,
                                    ),
                                  ),
                            title: Texts.body1(item.name),
                            subtitle: item.subtitle == null
                                ? null
                                : Texts.overline(
                                    item.subtitle ?? "-",
                                  ),
                          ),
                  ),
                ),
              );
            }).toList();
            return Row(children: childrenRow);
          },
        ).toList(),
        AnimatedSize(
          duration: const Duration(milliseconds: 300),
          child: !selectionController.isValid
              ? Padding(
                  padding: EdgeInsets.all(8.0.spMin),
                  child: Texts.overline(
                    "Pilih Salah Satu",
                    color: Get.theme.colorScheme.error,
                    textAlign: TextAlign.left,
                  ),
                )
              : const SizedBox(),
        ),
      ],
    );
  }
}
