import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerWidget {
  static Widget rectangular({
    required double? width,
    required double? height,
    Color baseColor = Colors.grey,
    Color highlightColor = Colors.white,
    double? borderRadius = 0,
    EdgeInsets? margin,
    EdgeInsets? padding,
    bool enabled = true,
  }) {
    return Shimmer.fromColors(
      enabled: enabled,
      baseColor: baseColor,
      highlightColor: highlightColor,
      child: Container(
        margin: margin,
        padding: padding,
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: Get.theme.shadowColor.withAlpha(150),
          borderRadius: BorderRadius.circular(borderRadius!),
        ),
      ),
    );
  }

  static Widget circular({
    required double? size,
    Color baseColor = Colors.grey,
    Color highlightColor = Colors.white,
    EdgeInsets? margin,
    EdgeInsets? padding,
    bool enabled = true,
  }) {
    return Shimmer.fromColors(
      enabled: enabled,
      baseColor: baseColor,
      highlightColor: highlightColor,
      child: Container(
        margin: margin,
        padding: padding,
        width: size,
        height: size,
        decoration: BoxDecoration(
          color: Get.theme.shadowColor.withAlpha(150),
          borderRadius: BorderRadius.circular(100),
        ),
      ),
    );
  }
}
