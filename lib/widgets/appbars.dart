import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'texts.dart';

class Appbars {
  static AppBar defaultAppbar({
    required String title,
    PreferredSizeWidget? bottom,
    List<Widget>? actions,
    bool backButton = false,
    bool centerTitle = true,
    bool gradientColorBackground = false,
    Widget? leading,
    double toolbarHeight = 56,
    List<Color>? gradientColors,
  }) {
    return AppBar(
      centerTitle: centerTitle,
      title: Texts.headline6(title),
      toolbarHeight: toolbarHeight,
      bottom: bottom,
      actions: actions,
      leading: backButton
          ? IconButton(
              onPressed: () => Get.back(),
              icon: Icon(
                Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
              ),
            )
          : leading,
      flexibleSpace: gradientColors != null
          ? Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  tileMode: TileMode.clamp,
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: gradientColors,
                ),
              ),
            )
          : Container(
              decoration:
                  BoxDecoration(color: Get.theme.appBarTheme.backgroundColor),
            ),
    );
  }
}
