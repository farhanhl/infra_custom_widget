import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:infra_custom_widget/widgets/texts.dart';
import 'package:intl/intl.dart';

import 'package:timeline_tile/timeline_tile.dart';

class TimelineItem {
  String? id;
  Widget title;
  bool active;
  IconData? iconData;
  DateTime? time;
  List<String>? listId;
  VoidCallback? onTap;
  TimelineItem({
    required this.title,
    this.time,
    this.active = false,
    this.iconData,
    this.id,
    this.listId,
    this.onTap,
  });

  String get dateString {
    var outputFormat = DateFormat('dd/MM/yyyy HH:mm');
    return time == null ? "" : outputFormat.format(time!.toLocal());
  }
}

class TimeLineWidget extends StatelessWidget {
  final List<TimelineItem> dataTimeline;
  final bool isHorizontal;
  final bool indicatorIndex;
  final bool hasTitle;
  final bool iconIndicator;
  final double indicatorSize;

  const TimeLineWidget({
    Key? key,
    required this.dataTimeline,
    this.isHorizontal = false,
    this.indicatorIndex = false,
    this.indicatorSize = 40.0,
    this.hasTitle = false,
    this.iconIndicator = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return isHorizontal
        ? Row(
            children: dataTimeline
                .asMap()
                .entries
                .map(
                  (entry) => Expanded(
                    child: TimelineTile(
                      axis: TimelineAxis.horizontal,
                      alignment: TimelineAlign.manual,
                      lineXY: 0.1,
                      isFirst: entry.key == 0,
                      isLast: entry.key == dataTimeline.length - 1,
                      indicatorStyle: IndicatorStyle(
                        width: indicatorSize.w,
                        height: indicatorSize.h,
                        color: entry.value.active
                            ? Get.theme.primaryColor
                            : Get.theme.shadowColor,
                        padding: EdgeInsets.all(10.spMin),
                        indicator: indicatorIndex
                            ? InkWell(
                                borderRadius: BorderRadius.circular(100.r),
                                onTap: entry.value.onTap,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: entry.value.active
                                        ? Get.theme.primaryColor
                                        : Get.theme.shadowColor,
                                    border: Border.all(
                                      color: entry.value.active
                                          ? Get.theme.primaryColor
                                          : Get.theme.colorScheme.background,
                                      width: .5.w,
                                    ),
                                    shape: BoxShape.circle,
                                  ),
                                  child: Center(
                                    child: Texts.body1(
                                      '${dataTimeline.indexOf(entry.value) + 1}',
                                    ),
                                  ),
                                ),
                              )
                            : iconIndicator
                                ? InkWell(
                                    borderRadius: BorderRadius.circular(100.r),
                                    onTap: entry.value.onTap,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: entry.value.active
                                            ? Get.theme.primaryColor
                                            : Get.theme.shadowColor
                                                .withAlpha(50),
                                        border: Border.all(
                                          color: entry.value.active
                                              ? Get.theme.primaryColor
                                              : Get
                                                  .theme.colorScheme.background,
                                          width: .5.w,
                                        ),
                                        shape: BoxShape.circle,
                                      ),
                                      child: Center(
                                        child: Icon(
                                          entry.value.iconData,
                                          color: entry.value.active
                                              ? null
                                              : Get.theme.shadowColor,
                                        ),
                                      ),
                                    ),
                                  )
                                : null,
                      ),
                      endChild: hasTitle
                          ? _RightChild(
                              icon: iconIndicator
                                  ? null
                                  : entry.value.iconData != null
                                      ? Icon(
                                          entry.value.iconData,
                                          color: entry.value.active
                                              ? Get.theme.primaryColor
                                              : Get.theme.shadowColor,
                                          size: 35.spMin,
                                        )
                                      : null,
                              title: entry.value.title,
                              message: entry.value.dateString,
                              isHorizontal: isHorizontal,
                            )
                          : const SizedBox(),
                      beforeLineStyle: LineStyle(
                        color: entry.value.active
                            ? Get.theme.primaryColor
                            : Get.theme.shadowColor.withAlpha(50),
                      ),
                    ),
                  ),
                )
                .toList(),
          )
        : Column(
            children: dataTimeline
                .asMap()
                .entries
                .map(
                  (entry) => TimelineTile(
                    alignment: TimelineAlign.manual,
                    lineXY: 0.1,
                    isFirst: entry.key == 0,
                    isLast: entry.key == dataTimeline.length - 1,
                    indicatorStyle: IndicatorStyle(
                      width: 20.w,
                      color: entry.value.active
                          ? Get.theme.primaryColor
                          : Get.theme.shadowColor,
                      padding: EdgeInsets.all(6.spMin),
                    ),
                    endChild: _RightChild(
                      icon: entry.value.iconData != null
                          ? Icon(
                              entry.value.iconData,
                              color: entry.value.active
                                  ? Get.theme.primaryColor
                                  : Get.theme.shadowColor,
                              size: 35.spMin,
                            )
                          : null,
                      title: entry.value.title,
                      message: entry.value.dateString,
                      isHorizontal: isHorizontal,
                    ),
                    beforeLineStyle: LineStyle(
                      color: entry.value.active
                          ? Get.theme.primaryColor
                          : Get.theme.shadowColor,
                    ),
                  ),
                )
                .toList(),
          );
  }
}

class _RightChild extends StatelessWidget {
  final Icon? icon;
  final Widget title;
  final String? message;
  final bool disabled;
  final bool isHorizontal;

  const _RightChild({
    Key? key,
    this.icon,
    required this.title,
    this.message,
    this.isHorizontal = false,
    // ignore: unused_element
    this.disabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return isHorizontal
        ? Column(
            children: [
              Opacity(
                opacity: disabled ? 0.5 : 1,
                child: icon,
              ),
              SizedBox(
                height: 5.h,
              ),
              message != null && message!.isNotEmpty
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        title,
                        SizedBox(height: 5.h),
                        Texts.overline(message!),
                      ],
                    )
                  : title,
            ],
          )
        : Row(
            children: [
              SizedBox(width: 10.w),
              Opacity(
                opacity: disabled ? 0.5 : 1,
                child: icon,
              ),
              SizedBox(width: 10.w),
              message != null && message!.isNotEmpty
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        title,
                        SizedBox(height: 5.h),
                        Texts.overline(message!),
                      ],
                    )
                  : title,
            ],
          );
  }
}
