import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  const MyButton({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text(
        "My Button",
      ),
    );
  }
}
