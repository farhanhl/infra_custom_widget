// ignore_for_file: file_names

import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QRViewExample extends StatefulWidget {
  const QRViewExample({Key? key}) : super(key: key);

  @override
  State<QRViewExample> createState() => _QRViewExampleState();
}

class _QRViewExampleState extends State<QRViewExample> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  QRViewController? qrController;
  Barcode? barcode;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: Appbars.defaultAppbar(title:title: 'Scan QR'),
      body: Stack(
        children: [
          buildQRView(context),
          // Positioned(
          //   top: 10.h,
          //   left: 10.w,
          //   child: IconButton(
          //     onPressed: () => Get.back(),
          //     icon: Platform.isIOS
          //         ? const Icon(Icons.arrow_back_ios)
          //         : const Icon(Icons.arrow_back),
          //   ),
          // ),
        ],
      ),
    );
  }

  Widget buildQRView(BuildContext context) {
    var scanArea =
        (Get.width.w < 400 || Get.height < 400) ? 150.0.spMin : 300.0.spMin;
    FocusManager.instance.primaryFocus?.unfocus();
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
        borderColor: Get.theme.primaryColor,
        borderRadius: 10.r,
        borderLength: 30.h,
        borderWidth: 10.w,
        overlayColor: Get.theme.primaryColor.withAlpha(100),
        cutOutSize: scanArea,
      ),
      onPermissionSet: (ctrl, p) => _onPermissionSet(context, ctrl, p),
    );
  }

  void _onQRViewCreated(QRViewController qrController) {
    setState(() {
      this.qrController = qrController;
      qrController.resumeCamera();
    });
    qrController.scannedDataStream.listen((result) {
      setState(() {
        qrController.dispose();
        barcode = result;
      });
    });
  }

  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    setState(() {
      log('${DateTime.now().toIso8601String()}_onPermissionSet $p');
      if (!p) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('no Permission')),
        );
      }
    });
  }

  @override
  void dispose() {
    qrController?.dispose();
    super.dispose();
  }

  void reasemble() async {
    if (Platform.isAndroid) {
      await qrController!.pauseCamera();
    }
    qrController!.resumeCamera();
  }
}
