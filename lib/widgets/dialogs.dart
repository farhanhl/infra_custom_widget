// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';
import 'package:lottie/lottie.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import '../core/utils/asset_helper.dart';
import 'buttons.dart';
import 'loading.dart';
import 'texts.dart';

///All Dialogs used in the App
class Dialogs {
  static Widget reminder({
    required String message,
    required String source,
    String btnText = "OK",
  }) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(15.r),
        ),
        color: Get.theme.colorScheme.background,
      ),
      padding: EdgeInsets.all(20.spMin),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.center,
            child: SvgPicture.asset(
              source,
              height: 100.h,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.h),
            child: Align(
              alignment: Alignment.center,
              child: Padding(
                padding: EdgeInsets.only(top: 10.h),
                child: Texts.subtitle2(
                  message,
                  textAlign: TextAlign.center,
                  textOverflow: TextOverflow.visible,
                ),
              ),
            ),
          ),
          btnText.isEmpty
              ? const SizedBox()
              : Align(
                  alignment: Alignment.center,
                  child: Buttons.defaultButton(
                    widget: Texts.subtitle2(btnText),
                    handler: () {
                      Get.back();
                    },
                  ),
                ),
        ],
      ),
    );
  }

  static Widget successAnimate({
    required String message,
    String source = AssetHelper.success,
    bool useButton = false,
  }) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(15.r),
        ),
        color: Get.theme.colorScheme.background,
      ),
      padding: EdgeInsets.all(20.spMin),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.center,
            child: Lottie.asset(
              source,
              height: 100.h,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.h),
            child: Align(
              alignment: Alignment.center,
              child: Padding(
                padding: EdgeInsets.only(top: 10.h),
                child: Texts.subtitle2(
                  message,
                  textAlign: TextAlign.center,
                  textOverflow: TextOverflow.visible,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20.h,
          ),
          useButton
              ? Align(
                  alignment: Alignment.center,
                  child: Buttons.defaultButton(
                    widget: Texts.subtitle2(
                      'OK',
                    ),
                    handler: () {
                      Get.back();
                    },
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  }

  static Widget errorAnimate({
    required String message,
    String source = AssetHelper.failed,
    bool useButton = false,
  }) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(15.r),
        ),
        color: Get.theme.colorScheme.background,
      ),
      padding: EdgeInsets.all(20.spMin),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.center,
            child: Lottie.asset(
              source,
              height: 100.h,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.h),
            child: Align(
              alignment: Alignment.center,
              child: Texts.caption(
                message,
                textAlign: TextAlign.center,
                textOverflow: TextOverflow.visible,
              ),
            ),
          ),
          SizedBox(
            height: 20.h,
          ),
          useButton
              ? Align(
                  alignment: Alignment.center,
                  child: Buttons.defaultButton(
                    widget: Texts.subtitle2(
                      'OK',
                    ),
                    handler: () {
                      Get.back();
                    },
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  }

  static Widget confirm({
    required String message,
    required String source,
  }) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(15)),
      ),
      padding: EdgeInsets.all(20.spMin),
      child: Wrap(
        children: [
          Align(
            alignment: Alignment.center,
            child: SvgPicture.asset(
              source,
              height: 100.h,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.h),
            child: Align(
              alignment: Alignment.center,
              child: Texts.subtitle2(
                message,
                textAlign: TextAlign.center,
                textOverflow: TextOverflow.visible,
              ),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Buttons.defaultButton(
              widget: Texts.subtitle2(
                'OK',
              ),
              handler: () {
                Get.back(result: true);
              },
            ),
          ),
        ],
      ),
    );
  }

  static Widget sliderDialog({
    required List<Widget> listWidget,
    bool useButton = false,
  }) {
    return Container(
      height: Get.height.h * 0.5.h,
      width: Get.width.w * 0.8.w,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(15.r),
        ),
        color: Get.theme.colorScheme.background,
      ),
      padding: EdgeInsets.all(20.spMin),
      child: PageView(
        children: listWidget,
      ),
    );
  }

  static Widget update({
    String? version,
    String? description,
    String? appLink,
    bool? allowDismissal,
    VoidCallback? onTap,
  }) {
    return Material(
      color: Colors.transparent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.zero,
            height: Get.height.h / 10.h,
            width: Get.width.w / 1.5.w,
            decoration: BoxDecoration(
              color: Get.theme.primaryColor,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.r),
                topRight: Radius.circular(10.r),
              ),
            ),
            child: Center(
              child: Icon(
                Icons.info,
                color: Get.theme.colorScheme.background,
                size: 40.spMin,
              ),
            ),
          ),
          Container(
            height: Get.height.h / 3.h,
            width: Get.width.w / 1.5.w,
            decoration: BoxDecoration(
              color: Get.theme.popupMenuTheme.color,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10.r),
                bottomRight: Radius.circular(10.r),
              ),
            ),
            child: Column(
              children: [
                Expanded(
                  flex: 3,
                  child: Padding(
                    padding: EdgeInsets.all(10.spMin),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Texts.body1('About Update'),
                            Texts.body2(version!, isItalic: true),
                          ],
                        ),
                        SizedBox(
                          height: 10.h,
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Html(data: description),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.all(10.spMin),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        allowDismissal!
                            ? Expanded(
                                child: Buttons.textButton(
                                  handler: Get.back,
                                  widget: Texts.button('Later'),
                                ),
                              )
                            : const SizedBox(),
                        SizedBox(
                          width: allowDismissal ? 20.w : 0.w,
                        ),
                        Expanded(
                          child: Buttons.defaultButton(
                            handler: onTap!,
                            widget: Texts.button('Update'),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  static Widget pin({
    required Function(String) onChanged,
    required int length,
    required VoidCallback onTap,
    required String buttonText,
    required bool isLoading,
    required bool isComplete,
    Function(String)? onCompleted,
    String description = 'Masukkan PIN Transaksi Anda',
    String buttonType = 'default',
    TextInputType keyboardType = TextInputType.number,
    bool obscureText = true,
    BorderRadius? borderRadius,
    Color? inactiveColor,
    Color? activeColor,
    PinCodeFieldShape pinCodeFieldShape = PinCodeFieldShape.box,
    MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
    CrossAxisAlignment crossAxisAlignmen = CrossAxisAlignment.stretch,
  }) {
    return Column(
      crossAxisAlignment: crossAxisAlignmen,
      mainAxisAlignment: mainAxisAlignment,
      children: [
        SizedBox(height: 10.h),
        Center(
          child: Texts.caption(description),
        ),
        SizedBox(height: 20.h),
        PinCodeTextField(
          appContext: Get.context!,
          length: 6,
          onChanged: (value) {},
          onCompleted: onCompleted,
          keyboardType: keyboardType,
          obscureText: obscureText,
          pinTheme: PinTheme(
            shape: pinCodeFieldShape,
            borderRadius: BorderRadius.circular(10.r),
            inactiveColor: Get.theme.shadowColor,
            activeColor: Get.theme.primaryColor,
            selectedColor: Get.theme.primaryColor,
          ),
        ),
        isLoading
            ? Padding(
                padding: EdgeInsets.only(top: 10.h),
                child: LoadingActivity.platformLoading(
                  color: Get.theme.primaryColor,
                  size: 40.sp,
                ),
              )
            : AbsorbPointer(
                absorbing: !isComplete,
                child: Buttons.defaultButton(
                  fillColor: isComplete
                      ? Get.theme.primaryColor
                      : Get.theme.shadowColor,
                  handler: onTap,
                  widget: Texts.button('Submit'),
                ),
              ),
      ],
    );
  }
}
