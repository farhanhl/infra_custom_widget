import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:infra_custom_widget/core/utils/controllers/date_and_time_picker_controller.dart';
import 'package:infra_custom_widget/core/utils/controllers/date_picker_controller.dart';
import 'package:infra_custom_widget/core/utils/controllers/textfield_controller.dart';
import 'package:infra_custom_widget/widgets/texts.dart';

class TextFields {
  static Widget defaultTextField({
    Key? key,
    required TextInputAction textInputAction,
    required TextInputType textInputType,
    required bool isLoading,
    required TextFieldController textFieldController,
    String? initialValue,
    IconData? iconData,
    bool hasInitialValue = false,
    bool readOnly = false,
    String? fontFamily,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        AnimatedSize(
          duration: const Duration(milliseconds: 300),
          child: Container(
            margin: EdgeInsets.only(
              top: 10.h,
            ),
            padding: EdgeInsets.only(left: 10.h, right: 10.h),
            decoration: BoxDecoration(
              color: readOnly
                  ? Colors.transparent
                  : Get.theme.colorScheme.background,
              borderRadius: BorderRadius.all(Radius.circular(8.r)),
              border: Border.all(
                width: 1.w,
                color: !textFieldController.isValid
                    ? Get.theme.colorScheme.error
                    : readOnly
                        ? Colors.grey
                        : Get.theme.shadowColor,
              ),
            ),
            child: TextFormField(
              style: GoogleFonts.getFont(
                fontFamily ?? (Platform.isIOS ? 'Open Sans' : 'Roboto'),
              ),
              readOnly: readOnly,
              cursorColor: Get.theme.primaryColor,
              inputFormatters: textFieldController.inputFormatter ?? [],
              initialValue: textFieldController.text,
              enabled: !isLoading,
              obscureText: textFieldController.obscureText &&
                  !textFieldController.visible,
              textInputAction: textInputAction,
              keyboardType: textInputType,
              onChanged: textFieldController.onChange,
              decoration: InputDecoration(
                hintStyle: GoogleFonts.getFont(
                  fontFamily ?? (Platform.isIOS ? 'Open Sans' : 'Roboto'),
                ),
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                contentPadding: textFieldController.obscureText ||
                        textFieldController.visible
                    ? EdgeInsets.only(
                        top: 14.h,
                        bottom: 10.h,
                        left: 10.w,
                        right: 10.w,
                      )
                    : EdgeInsets.all(10.spMin),
                // labelText: textFieldController.title,
                hintText: textFieldController.title,
                icon: iconData == null
                    ? null
                    : Icon(
                        iconData,
                        color: Get.theme.primaryColor,
                      ),
                filled: true,
                fillColor: Colors.transparent,

                suffixIcon: textFieldController.obscureText ||
                        textFieldController.visible
                    ? IconButton(
                        padding: EdgeInsets.zero,
                        icon: Icon(
                          textFieldController.visible
                              ? Icons.visibility
                              : Icons.visibility_off,
                          // color: Colors.red,
                        ),
                        onPressed: textFieldController.toogleVisible,
                        splashRadius: 24.r,
                      )
                    : null,
              ),
            ),
          ),
        ),
        AnimatedSize(
          duration: const Duration(milliseconds: 300),
          child: !textFieldController.isValid
              ? Padding(
                  padding: EdgeInsets.all(8.0.spMin),
                  child: Texts.caption(
                    textFieldController.errorMessage,
                    color: Get.theme.colorScheme.error,
                    textOverflow: TextOverflow.visible,
                  ),
                )
              : const SizedBox(),
        ),
      ],
    );
  }

  static Widget dateTextField({
    Key? key,
    required TextInputAction textInputAction,
    required TextInputType textInputType,
    required bool isLoading,
    required DatePickerController datePickerController,
    IconData? iconData,
    String? fontFamily,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        AnimatedSize(
          duration: const Duration(milliseconds: 300),
          child: Container(
            margin: EdgeInsets.only(
              top: 10.h,
            ),
            padding: EdgeInsets.only(left: 10.w, right: 10.w),
            decoration: BoxDecoration(
              color: Get.theme.colorScheme.background,
              borderRadius: BorderRadius.all(Radius.circular(10.r)),
              border: Border.all(
                width: 1.w,
                color: !datePickerController.isValid
                    ? Colors.red
                    : Get.theme.shadowColor,
              ),
            ),
            child: TextFormField(
              // initialValue: datePickerController.text,
              onTap: iconData == null
                  ? () async {
                      final DateTime? picked = await showDatePicker(
                        context: Get.context!,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(1945, 8),
                        lastDate: DateTime(2101),
                        builder: (context, child) {
                          return Theme(
                            data: Get.theme.brightness == Brightness.light
                                ? Get.theme.copyWith(
                                    // Light Theme
                                    colorScheme: ColorScheme.light(
                                      primary: Get
                                          .theme.primaryColor, // <-- SEE HERE
                                      // onPrimary: Get.theme., // <-- SEE HERE
                                      onSurface: Get.theme.textTheme.bodyLarge!
                                          .color!, // <-- SEE HERE
                                    ),
                                    textButtonTheme: TextButtonThemeData(
                                      style: TextButton.styleFrom(
                                        foregroundColor: Get.theme
                                            .primaryColor, // button text color
                                      ),
                                    ),
                                  )
                                : Get.theme.copyWith(
                                    // Dark Theme
                                    colorScheme: ColorScheme.light(
                                      primary: Get
                                          .theme.primaryColor, // <-- SEE HERE
                                      // onPrimary: Colors.amber, // <-- SEE HERE
                                      onSurface: Get.theme.textTheme.bodyLarge!
                                          .color!, // <-- SEE HERE
                                    ),
                                    textButtonTheme: TextButtonThemeData(
                                      style: TextButton.styleFrom(
                                        foregroundColor: Get.theme
                                            .primaryColor, // button text color
                                      ),
                                    ),
                                  ),
                            child: child!,
                          );
                        },
                      );
                      if (picked != null) {
                        datePickerController.changeDate(picked);
                      }
                    }
                  : null,
              controller: datePickerController.textEditingController,
              readOnly: true,
              textInputAction: textInputAction,
              keyboardType: textInputType,
              style: GoogleFonts.getFont(
                fontFamily ?? (Platform.isIOS ? 'Open Sans' : 'Roboto'),
              ),
              decoration: InputDecoration(
                hintStyle: GoogleFonts.getFont(
                  fontFamily ?? (Platform.isIOS ? 'Open Sans' : 'Roboto'),
                ),
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                contentPadding:
                    EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
                hintText: datePickerController.title,
                icon: iconData == null
                    ? null
                    : Icon(
                        iconData,
                      ),
                filled: true,
                fillColor: Colors.transparent,
                suffixIcon: IconButton(
                  icon: iconData == null ? const SizedBox() : Icon(iconData),
                  onPressed: iconData == null
                      ? () {}
                      : () async {
                          final DateTime? picked = await showDatePicker(
                            context: Get.context!,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(2015, 8),
                            lastDate: DateTime(2101),
                            builder: (context, child) {
                              return Theme(
                                data: Get.theme.brightness == Brightness.light
                                    ? Get.theme.copyWith(
                                        // Light Theme
                                        colorScheme: ColorScheme.light(
                                          primary: Get.theme
                                              .primaryColor, // <-- SEE HERE
                                          // onPrimary: Get.theme., // <-- SEE HERE
                                          // onSurface: Colors.blueAccent, // <-- SEE HERE
                                        ),
                                        textButtonTheme: TextButtonThemeData(
                                          style: TextButton.styleFrom(
                                            foregroundColor: Get.theme
                                                .primaryColor, // button text color
                                          ),
                                        ),
                                      )
                                    : Get.theme.copyWith(
                                        // Dark Theme
                                        colorScheme: ColorScheme.light(
                                          primary: Get.theme
                                              .primaryColor, // <-- SEE HERE
                                          // onPrimary: Colors.amber, // <-- SEE HERE
                                          onSurface:
                                              Colors.white, // <-- SEE HERE
                                        ),
                                        textButtonTheme: TextButtonThemeData(
                                          style: TextButton.styleFrom(
                                            foregroundColor: Get.theme
                                                .primaryColor, // button text color
                                          ),
                                        ),
                                      ),
                                child: child!,
                              );
                            },
                          );
                          if (picked != null) {
                            datePickerController.changeDate(picked);
                          }
                        },
                  splashRadius: 24.r,
                ),
              ),
            ),
          ),
        ),
        AnimatedSize(
          duration: const Duration(milliseconds: 300),
          child: !datePickerController.isValid
              ? Padding(
                  padding: EdgeInsets.all(8.0.spMin),
                  child: Texts.overline(
                    datePickerController.errorMessage,
                    color: Get.theme.colorScheme.error,
                    textOverflow: TextOverflow.visible,
                  ),
                )
              : const SizedBox(),
        ),
      ],
    );
  }

  static Widget dateAndTimeTextField({
    Key? key,
    required TextInputAction textInputAction,
    required TextInputType textInputType,
    required bool isLoading,
    required DateAndTimePickerController dateAndTimePickerController,
    String? initialValue,
    IconData? iconData,
    String? fontFamily,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        AnimatedSize(
          duration: const Duration(milliseconds: 300),
          child: Container(
            margin: EdgeInsets.only(
              top: 10.h,
            ),
            padding: EdgeInsets.only(left: 10.w, right: 10.w),
            decoration: BoxDecoration(
              color: Get.theme.scaffoldBackgroundColor,
              borderRadius: BorderRadius.all(Radius.circular(10.r)),
              border: Border.all(
                width: 1.w,
                color: !dateAndTimePickerController.isValid
                    ? Colors.red
                    : Get.theme.shadowColor,
              ),
            ),
            child: TextFormField(
              style: GoogleFonts.getFont(
                fontFamily ?? (Platform.isIOS ? 'Open Sans' : 'Roboto'),
              ),
              controller: dateAndTimePickerController.textEditingController,
              readOnly: true,
              textInputAction: textInputAction,
              keyboardType: textInputType,
              decoration: InputDecoration(
                hintStyle: GoogleFonts.getFont(
                  fontFamily ?? (Platform.isIOS ? 'Open Sans' : 'Roboto'),
                ),
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                contentPadding:
                    EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
                hintText: dateAndTimePickerController.title,
                icon: iconData == null
                    ? null
                    : Icon(
                        iconData,
                      ),
                filled: true,
                fillColor: Get.theme.colorScheme.background,
                suffixIcon: IconButton(
                  icon: const Icon(
                    Icons.date_range,
                  ),
                  onPressed: () async {
                    final DateTime? datePick = await showDatePicker(
                      context: Get.context!,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2015, 8),
                      lastDate: DateTime(2101),
                      builder: (context, child) {
                        return Theme(
                          data: Get.theme.brightness == Brightness.light
                              ? Get.theme.copyWith(
                                  // Light Theme
                                  colorScheme: ColorScheme.light(
                                    primary:
                                        Get.theme.primaryColor, // <-- SEE HERE
                                    // onPrimary: Get.theme., // <-- SEE HERE
                                    // onSurface: Colors.blueAccent, // <-- SEE HERE
                                  ),
                                  textButtonTheme: TextButtonThemeData(
                                    style: TextButton.styleFrom(
                                      foregroundColor: Get.theme
                                          .primaryColor, // button text color
                                    ),
                                  ),
                                )
                              : Get.theme.copyWith(
                                  // Dark Theme
                                  colorScheme: ColorScheme.light(
                                    primary:
                                        Get.theme.primaryColor, // <-- SEE HERE
                                    // onPrimary: Colors.amber, // <-- SEE HERE
                                    onSurface: Colors.white, // <-- SEE HERE
                                  ),
                                  textButtonTheme: TextButtonThemeData(
                                    style: TextButton.styleFrom(
                                      foregroundColor: Get.theme
                                          .primaryColor, // button text color
                                    ),
                                  ),
                                ),
                          child: child!,
                        );
                      },
                    );

                    final TimeOfDay? timePick = await showTimePicker(
                      context: Get.context!,
                      initialTime: TimeOfDay.now(),
                      builder: (context, child) => MediaQuery(
                        data: MediaQuery.of(context)
                            .copyWith(alwaysUse24HourFormat: true),
                        child: child ?? Container(),
                      ),
                    );

                    if (datePick != null && timePick != null) {
                      dateAndTimePickerController.changeDate(
                        datePick,
                        timePick,
                      );
                    }
                  },
                  splashRadius: 24.r,
                ),
              ),
            ),
          ),
        ),
        AnimatedSize(
          duration: const Duration(milliseconds: 300),
          child: !dateAndTimePickerController.isValid
              ? Padding(
                  padding: EdgeInsets.all(8.0.spMin),
                  child: Texts.overline(
                    dateAndTimePickerController.errorMessage,
                    color: Get.theme.colorScheme.error,
                    textOverflow: TextOverflow.visible,
                  ),
                )
              : const SizedBox(),
        ),
      ],
    );
  }

  static Widget multilineTextField({
    Key? key,
    required TextInputAction textInputAction,
    required TextInputType textInputType,
    required bool isLoading,
    required TextFieldController textFieldController,
    IconData? iconData,
    String? initialValue,
    int minLines = 3,
    int maxLines = 5,
    String? fontFamily,
  }) {
    return AnimatedSize(
      duration: const Duration(milliseconds: 300),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(
              top: 10.h,
            ),
            padding: EdgeInsets.symmetric(horizontal: 8.w),
            decoration: BoxDecoration(
              color: Get.theme.colorScheme.background,
              borderRadius: BorderRadius.all(Radius.circular(10.r)),
              border: Border.all(
                width: 1,
                color: !textFieldController.isValid
                    ? Get.theme.colorScheme.error
                    : Get.theme.shadowColor,
              ),
            ),
            child: TextFormField(
              style: GoogleFonts.getFont(
                fontFamily ?? (Platform.isIOS ? 'Open Sans' : 'Roboto'),
              ),
              initialValue: textFieldController.text,
              enabled: !isLoading,
              obscureText: textFieldController.obscureText &&
                  !textFieldController.visible,
              textInputAction: textInputAction,
              keyboardType: textInputType,
              onChanged: textFieldController.onChange,
              minLines: minLines,
              maxLines: maxLines,
              decoration: InputDecoration(
                hintStyle: GoogleFonts.getFont(
                  fontFamily ?? (Platform.isIOS ? 'Open Sans' : 'Roboto'),
                ),
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                contentPadding:
                    EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
                hintText: textFieldController.title,
                icon: iconData == null
                    ? null
                    : Icon(
                        iconData,
                      ),
                filled: true,
                fillColor: Colors.transparent,
              ),
            ),
          ),
          AnimatedSize(
            duration: const Duration(milliseconds: 300),
            child: !textFieldController.isValid
                ? Padding(
                    padding: EdgeInsets.all(8.0.spMin),
                    child: Texts.overline(
                      textFieldController.errorMessage,
                      color: Get.theme.colorScheme.error,
                      textOverflow: TextOverflow.visible,
                    ),
                  )
                : const SizedBox(),
          ),
        ],
      ),
    );
  }
}
