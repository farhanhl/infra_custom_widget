// ignore_for_file: deprecated_member_use

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:photo_view/photo_view.dart';
import 'package:share_plus/share_plus.dart';
import 'package:http/http.dart';

import '../data/helper/image_upload_model.dart';

class HeroPhotoViewRouteWrapper extends StatelessWidget {
  const HeroPhotoViewRouteWrapper({
    Key? key,
    required this.imageProvider,
    required this.tag,
    this.backgroundDecoration,
  }) : super(key: key);

  final ImageProvider imageProvider;
  final String tag;
  final BoxDecoration? backgroundDecoration;

  @override
  Widget build(BuildContext context) {
    ImageUploadModel imageUploadModel = Get.arguments;
    return SafeArea(
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            constraints: BoxConstraints.expand(
              height: Get.height.h,
            ),
            child: PhotoView(
              imageProvider: imageProvider,
              backgroundDecoration: backgroundDecoration,
              heroAttributes: PhotoViewHeroAttributes(tag: tag),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Material(
                color: Colors.transparent,
                child: PlatformIconButton(
                  onPressed: Get.back,
                  icon: Icon(
                    Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
                    color: Get.theme.colorScheme.background,
                  ),
                ),
              ),
              Material(
                color: Colors.transparent,
                child: PlatformIconButton(
                  onPressed: () async {
                    final urlImage = imageUploadModel.imageUrl;
                    final url = Uri.parse(urlImage!);
                    final response = await get(url);
                    final bytes = response.bodyBytes;

                    final temp = await getTemporaryDirectory();
                    final path = '${temp.path}/image.jpg';
                    debugPrint(path);
                    File(path).writeAsBytesSync(bytes);

                    debugPrint(urlImage);

                    await Share.shareFiles([path], text: 'Cek Gambar Ini.');
                  },
                  icon: Icon(
                    Icons.share,
                    color: Get.theme.colorScheme.background,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
