import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../data/helper/image_upload_model.dart';
import 'image_viewer.dart';
import './texts.dart';

class Cards {
  static Widget menuCard(
    IconCardItem iconCard,
    VoidCallback functionHanler, {
    bool useCard = true,
    bool isSuspend = false,
    VoidCallback? suspendHanler,
  }) {
    final width = Get.width.w;

    return Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: Get.theme.primaryColor.withAlpha(100),
        onTap: functionHanler,
        // onTap: isSuspend ? suspendHanler : functionHanler,
        child: FittedBox(
          fit: BoxFit.scaleDown,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                decoration: !useCard
                    ? null
                    : BoxDecoration(
                        color: Get.theme.colorScheme.background,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                            color: Get.theme.shadowColor,
                            offset: const Offset(0.0, 1.0), //(x,y)
                            blurRadius: 1.0,
                          ),
                        ],
                      ),
                foregroundDecoration: !isSuspend
                    ? null
                    : const BoxDecoration(
                        color: Colors.blueGrey,
                        backgroundBlendMode: BlendMode.saturation,
                      ),
                child: Padding(
                  padding:
                      useCard ? const EdgeInsets.all(10.0) : EdgeInsets.zero,
                  child: iconCard.isIcon
                      ? Icon(
                          iconCard.iconData,
                          color: Get.theme.primaryColor,
                          size: 30,
                        )
                      : Image(
                          width: width / 10,
                          height: width / 10,
                          image: AssetImage(iconCard.imageAsset),
                        ),
                ),
              ),
              useCard
                  ? const SizedBox(
                      height: 5,
                    )
                  : const SizedBox(
                      height: 3,
                    ),
              Texts.overline(
                iconCard.label,
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }

  static Widget documentCard(
    ImageUploadModel imageUploadModel,
    VoidCallback clickHandler, {
    bool useBorder = true,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(top: 10.h),
          decoration: BoxDecoration(
            color: Get.theme.colorScheme.background,
            borderRadius: BorderRadius.all(Radius.circular(10.r)),
            border: !useBorder
                ? null
                : Border.all(
                    width: 1.w,
                    color: imageUploadModel.valid
                        ? Get.theme.shadowColor
                        : Get.theme.colorScheme.error,
                  ),
            boxShadow: useBorder
                ? null
                : [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.07),
                      offset: const Offset(0.0, 1.0), //(x,y)
                      blurRadius: 6.0,
                    ),
                  ],
          ),

          //   padding: EdgeInsets.all(20),
          child: ListTile(
            tileColor: Colors.transparent,
            visualDensity: VisualDensity.compact,
            onTap: () {
              if (imageUploadModel.file == null &&
                  imageUploadModel.imageUrl == null) return;
              Get.to(
                  () => HeroPhotoViewRouteWrapper(
                        imageProvider: imageUploadModel.sourceType ==
                                    ImageSourceType.xfile &&
                                imageUploadModel.file != null
                            ? FileImage(
                                File(imageUploadModel.file!.path),
                              )
                            : CachedNetworkImageProvider(
                                imageUploadModel.imageUrl!) as ImageProvider,
                        tag: imageUploadModel.id,
                      ),
                  arguments: imageUploadModel);
            },
            contentPadding: imageUploadModel.text == null
                ? const EdgeInsets.symmetric(vertical: 8, horizontal: 16)
                : null,
            leading: imageUploadModel.file == null &&
                    imageUploadModel.imageUrl == null
                ? const SizedBox(
                    width: 100,
                    child: Icon(
                      Icons.image_not_supported,
                      size: 50,
                      color: Colors.grey,
                    ),
                  )
                : SizedBox(
                    width: 100,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Hero(
                          tag: imageUploadModel.id,
                          child: imageUploadModel.sourceType ==
                                      ImageSourceType.xfile &&
                                  imageUploadModel.file != null
                              ? Image.file(
                                  File(imageUploadModel.file!.path),
                                  fit: BoxFit.scaleDown,
                                  height: 50,
                                )
                              : Image.network(
                                  imageUploadModel.imageUrl!,
                                  fit: BoxFit.scaleDown,
                                  height: 50,
                                )),
                    ),
                  ),
            title: Texts.caption(
              imageUploadModel.title,
              textOverflow: TextOverflow.visible,
            ),
            subtitle: imageUploadModel.text == null
                ? null
                : Texts.overline(
                    imageUploadModel.text ?? "",
                    textOverflow: TextOverflow.visible,
                  ),
            trailing: IconButton(
              onPressed: clickHandler,
              icon: const Icon(Icons.add_a_photo),
            ),
          ),
        ),
        AnimatedSize(
          duration: const Duration(milliseconds: 300),
          child: !imageUploadModel.valid
              ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Texts.overline(
                    "Gambar wajib disertakan",
                    color: Get.theme.colorScheme.error,
                  ),
                )
              : const SizedBox(
                  height: 10,
                ),
        ),
      ],
    );
  }

  static Widget bottomSheetMenu(
    IconCardItem iconCard,
    VoidCallback functionHandler, {
    bool useCard = true,
    bool isSuspend = false,
    VoidCallback? suspendHandler,
  }) {
    return Column(
      children: [
        Container(
          decoration: !useCard
              ? null
              : BoxDecoration(
                  shape: BoxShape.circle,
                  color: Get.theme.colorScheme.background,
                  boxShadow: [
                      BoxShadow(
                        color: Get.theme.shadowColor,
                        offset: const Offset(0.0, 1.0), //(x,y)
                        blurRadius: 1.0,
                      ),
                    ]),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(100.r),
              onTap: isSuspend ? suspendHandler : functionHandler,
              child: Container(
                  padding: EdgeInsets.all(10.spMin),
                  decoration: const BoxDecoration(
                    color: Colors.transparent,
                  ),
                  child:
                      // iconCard.isIcon
                      //     ?
                      Icon(
                    iconCard.iconData,
                    color: Get.theme.primaryColor,
                    size: 50.spMin,
                  )
                  // : Image(
                  //     width: Get.width / 10.w,
                  //     height: Get.width / 10.h,
                  //     image: AssetImage(iconCard.imageAsset),
                  //   ),
                  ),
            ),
          ),
        ),
        useCard
            ? const SizedBox(
                height: 15,
              )
            : const SizedBox(
                height: 3,
              ),
        Texts.overline(
          iconCard.label,
          textAlign: TextAlign.center,
        ),
      ],
    );
  }

  static Widget previewImage(
    ImageUploadModel imageUploadModel, {
    double? height,
    double? width,
    EdgeInsets? padding,
    EdgeInsets? margin,
  }) {
    return Container(
      child: imageUploadModel.file == null && imageUploadModel.imageUrl == null
          ? Padding(
              padding: EdgeInsets.only(top: 10.0.h),
              child: Stack(
                alignment: Alignment.topCenter,
                children: [
                  Container(
                    width: Get.width.w,
                    height: Get.width.w / 2,
                    decoration: BoxDecoration(
                      color: Get.theme.shadowColor.withAlpha(75),
                      borderRadius: BorderRadius.circular(10.r),
                    ),
                    child: Icon(
                      Icons.broken_image,
                      color: Get.theme.shadowColor,
                    ),
                  ),
                  Container(
                    width: Get.width.w,
                    padding: EdgeInsets.symmetric(vertical: 5.h),
                    decoration: BoxDecoration(
                      color: Get.theme.shadowColor.withAlpha(100),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10.r),
                        topRight: Radius.circular(10.r),
                      ),
                    ),
                    child: Center(
                      child: Texts.caption(imageUploadModel.title),
                    ),
                  ),
                ],
              ),
            )
          : InkWell(
              onTap: () {
                if (imageUploadModel.file == null &&
                    imageUploadModel.imageUrl == null) return;
                Get.to(
                    () => HeroPhotoViewRouteWrapper(
                          imageProvider: imageUploadModel.sourceType ==
                                      ImageSourceType.xfile &&
                                  imageUploadModel.file != null
                              ? FileImage(
                                  File(imageUploadModel.file!.path),
                                )
                              : CachedNetworkImageProvider(
                                  imageUploadModel.imageUrl!) as ImageProvider,
                          tag: imageUploadModel.id,
                        ),
                    arguments: imageUploadModel);
              },
              child: Padding(
                padding: EdgeInsets.only(top: 10.h),
                child: Column(
                  children: [
                    Container(
                      width: Get.width.w,
                      padding: EdgeInsets.symmetric(vertical: 5.h),
                      decoration: BoxDecoration(
                        color: Get.theme.primaryColor.withAlpha(100),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10.r),
                          topRight: Radius.circular(10.r),
                        ),
                      ),
                      child: Center(
                        child: Texts.caption(imageUploadModel.title),
                      ),
                    ),
                    SizedBox(
                      width: Get.width.w,
                      height: Get.width.w / 2,
                      child: Hero(
                        tag: imageUploadModel.id,
                        child: imageUploadModel.sourceType ==
                                    ImageSourceType.xfile &&
                                imageUploadModel.file != null
                            ? ClipRRect(
                                borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(10.r),
                                  bottomRight: Radius.circular(10.r),
                                ),
                                child: Image.file(
                                  File(imageUploadModel.file!.path),
                                  fit: BoxFit.fill,
                                  height: 50,
                                ),
                              )
                            : ClipRRect(
                                borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(10.r),
                                  bottomRight: Radius.circular(10.r),
                                ),
                                child: Image.network(
                                  imageUploadModel.imageUrl!,
                                  fit: BoxFit.cover,
                                  height: 50,
                                ),
                              ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}

class IconCardItem {
  final String imageAsset;
  final String label;
  final IconData? iconData;
  final bool isIcon;

  IconCardItem({
    required this.imageAsset,
    required this.label,
    this.iconData,
    this.isIcon = false,
  });
}
