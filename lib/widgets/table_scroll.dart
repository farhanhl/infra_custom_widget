import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import './texts.dart';

class TableTransactionItem {
  DateTime date;
  double capacity;
  double saleAmount;
  double salePrice;
  TableTransactionItem({
    required this.date,
    required this.capacity,
    required this.saleAmount,
    required this.salePrice,
  });

  var datetFormat = DateFormat(
    'dd/MM/yy',
  );
  var formatter = NumberFormat("#,##0", "id");

  String get dateString {
    return datetFormat.format(date);
  }

  String get capacityString {
    return formatter.format(capacity);
  }

  String get saleAmountString {
    return formatter.format(saleAmount);
  }

  String get salePriceString {
    return formatter.format(salePrice);
  }
}

class TableScroll extends StatefulWidget {
  final List<TableTransactionItem> transaction;
  final bool isFull;
  const TableScroll({Key? key, required this.transaction, required this.isFull})
      : super(key: key);
  @override
  // ignore: library_private_types_in_public_api
  _TableScrollState createState() => _TableScrollState();
}

class _TableScrollState extends State<TableScroll> {
  bool _sortDateAsc = true;
  bool _sortSaleAmountAsc = true;
  bool _sortSalePriceAsc = true;
  bool _sortSaleCapacityAsc = true;
  bool _sortAsc = true;
  int? _sortColumnIndex;
  List<TableTransactionItem> _transaction = [];
  bool _isFull = true;

  @override
  initState() {
    super.initState();
    setState(() {
      _transaction = [...widget.transaction];
      _isFull = widget.isFull;
    });
  }

  @override
  Widget build(BuildContext context) {
    var myColumns = _isFull
        ? [
            DataColumn(
              label: Texts.subtitle2(
                "Tanggal",
                color: Get.theme.colorScheme.background,
              ),
              onSort: (columnIndex, sortAscending) {
                setState(() {
                  if (columnIndex == _sortColumnIndex) {
                    _sortAsc = _sortDateAsc = sortAscending;
                  } else {
                    _sortColumnIndex = columnIndex;
                    _sortAsc = _sortDateAsc;
                  }
                  _transaction.sort((a, b) => a.date.compareTo(b.date));
                  if (!_sortAsc) {
                    _transaction = _transaction.reversed.toList();
                  }
                });
              },
            ),
            DataColumn(
              numeric: true,
              label: Texts.subtitle2(
                "Penjualan (L)",
                color: Get.theme.colorScheme.background,
              ),
              onSort: (columnIndex, sortAscending) {
                setState(() {
                  if (columnIndex == _sortColumnIndex) {
                    _sortAsc = _sortSaleAmountAsc = sortAscending;
                  } else {
                    _sortColumnIndex = columnIndex;
                    _sortAsc = _sortSaleAmountAsc;
                  }
                  _transaction
                      .sort((a, b) => a.saleAmount.compareTo(b.saleAmount));
                  if (!_sortAsc) {
                    _transaction = _transaction.reversed.toList();
                  }
                });
              },
            ),
            DataColumn(
              label: Texts.subtitle2(
                "Penjualan (Rp)",
                color: Get.theme.colorScheme.background,
              ),
              numeric: true,
              onSort: (columnIndex, sortAscending) {
                setState(() {
                  if (columnIndex == _sortColumnIndex) {
                    _sortAsc = _sortSalePriceAsc = sortAscending;
                  } else {
                    _sortColumnIndex = columnIndex;
                    _sortAsc = _sortSalePriceAsc;
                  }
                  _transaction
                      .sort((a, b) => a.salePrice.compareTo(b.salePrice));
                  if (!_sortAsc) {
                    _transaction = _transaction.reversed.toList();
                  }
                });
              },
            ),
            DataColumn(
              label: Texts.subtitle2(
                "Stok (L)",
                color: Get.theme.colorScheme.background,
              ),
              numeric: true,
              onSort: (columnIndex, sortAscending) {
                setState(() {
                  if (columnIndex == _sortColumnIndex) {
                    _sortAsc = _sortSaleCapacityAsc = sortAscending;
                  } else {
                    _sortColumnIndex = columnIndex;
                    _sortAsc = _sortSaleCapacityAsc;
                  }
                  _transaction.sort((a, b) => a.capacity.compareTo(b.capacity));
                  if (!_sortAsc) {
                    _transaction = _transaction.reversed.toList();
                  }
                });
              },
            ),
          ]
        : [
            DataColumn(
              label: Texts.subtitle2(
                "Tanggal",
                color: Get.theme.colorScheme.background,
              ),
              onSort: (columnIndex, sortAscending) {
                setState(() {
                  if (columnIndex == _sortColumnIndex) {
                    _sortAsc = _sortDateAsc = sortAscending;
                  } else {
                    _sortColumnIndex = columnIndex;
                    _sortAsc = _sortDateAsc;
                  }
                  _transaction.sort((a, b) => a.date.compareTo(b.date));
                  if (!_sortAsc) {
                    _transaction = _transaction.reversed.toList();
                  }
                });
              },
            ),
            DataColumn(
              numeric: true,
              label: Texts.subtitle2(
                "Penjualan (L)",
                color: Get.theme.colorScheme.background,
              ),
              onSort: (columnIndex, sortAscending) {
                setState(() {
                  if (columnIndex == _sortColumnIndex) {
                    _sortAsc = _sortSaleAmountAsc = sortAscending;
                  } else {
                    _sortColumnIndex = columnIndex;
                    _sortAsc = _sortSaleAmountAsc;
                  }
                  _transaction
                      .sort((a, b) => a.saleAmount.compareTo(b.saleAmount));
                  if (!_sortAsc) {
                    _transaction = _transaction.reversed.toList();
                  }
                });
              },
            ),
            DataColumn(
              label: Texts.subtitle2(
                "Stok (L)",
                color: Get.theme.colorScheme.background,
              ),
              numeric: true,
              onSort: (columnIndex, sortAscending) {
                setState(() {
                  if (columnIndex == _sortColumnIndex) {
                    _sortAsc = _sortSaleCapacityAsc = sortAscending;
                  } else {
                    _sortColumnIndex = columnIndex;
                    _sortAsc = _sortSaleCapacityAsc;
                  }
                  _transaction.sort((a, b) => a.capacity.compareTo(b.capacity));
                  if (!_sortAsc) {
                    _transaction = _transaction.reversed.toList();
                  }
                });
              },
            ),
          ];

    var myRows = _transaction.map(
      (transaction) {
        return DataRow(
          cells: _isFull
              ? [
                  DataCell(
                    Text(
                      transaction.dateString,
                    ),
                  ),
                  DataCell(
                    Text(
                      transaction.saleAmountString,
                    ),
                  ),
                  DataCell(
                    Text(
                      transaction.salePriceString,
                    ),
                  ),
                  DataCell(
                    Text(
                      transaction.capacityString,
                    ),
                  ),
                ]
              : [
                  DataCell(
                    Text(
                      transaction.dateString,
                    ),
                  ),
                  DataCell(
                    Text(
                      transaction.saleAmountString,
                    ),
                  ),
                  DataCell(
                    Text(
                      transaction.capacityString,
                    ),
                  ),
                ],
        );
      },
    );
    return DataTable(
      headingRowColor: MaterialStateColor.resolveWith(
        (states) => Get.theme.primaryColor,
      ),
      headingRowHeight: 40,
      showBottomBorder: true,
      columnSpacing: 5,
      horizontalMargin: 5,
      columns: myColumns,
      rows: myRows.toList(),
      sortColumnIndex: _sortColumnIndex,
      sortAscending: _sortAsc,
    );
  }
}
