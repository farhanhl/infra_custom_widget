import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:intl/intl.dart';

const kBackgroundColor = Color(0xFFF8F8F8);
const kActiveIconColor = Color(0xFFE68342);
const kTextColor = Color(0xFF222B45);
const kBlueLightColor = Color(0xFFC7B8F5);
const kBlueColor = Color(0xFF817DC0);
const kShadowColor = Color(0xFFE6E6E6);

final NumberFormat formatterThousand = NumberFormat("#,##0", "id");
final NumberFormat formatterThousandWithDecimal =
    NumberFormat("#,##0.###", "id");
final DateFormat formatterDate = DateFormat('dd-MM-yyyy');
final DateFormat formatterDateAndTime = DateFormat('dd-MM-yyyy HH:mm:ss');
final DateFormat formatterTime = DateFormat('HH:mm');
final DateFormat formatterTimeWithSecond = DateFormat('HH:mm:ss');
final DateFormat formatterDateSlash = DateFormat('dd/MM/yyyy');
final DateFormat formatterDateAndTimeSlash = DateFormat('dd/MM/yyyy HH.mm.ss');
final DateFormat formatterDateNoDash = DateFormat('dd MMMM yyyy');
