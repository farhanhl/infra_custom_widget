// ignore: file_names

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateAndTimePickerController {
  String title;
  final String errorMessage;
  final VoidCallback _updateFunc;
  final List<bool Function(String)> validationFunc;

  bool isValid = true;
  DateTime? selectedDate;
  TextEditingController textEditingController = TextEditingController();

  DateAndTimePickerController(
    this.title,
    this.validationFunc,
    this.errorMessage,
    this._updateFunc,
  );

  final DateFormat formatter = DateFormat('dd-MM-yyyy HH:mm');

  dispose() {
    textEditingController.dispose();
  }

  changeDate(DateTime dateTime, TimeOfDay timeOfDay) {
    dateTime = DateTime(
      dateTime.year,
      dateTime.month,
      dateTime.day,
      timeOfDay.hour,
      timeOfDay.minute,
    );
    selectedDate = dateTime;
    textEditingController.text = formatter.format(dateTime.toLocal());
    _updateFunc();
  }

  DateAndTimePickerController copyWith({
    String? title,
    List<bool Function(String)>? validationFunc,
    String? errorMessage,
    bool? obscureText,
  }) {
    return DateAndTimePickerController(
      title ?? this.title,
      validationFunc ?? this.validationFunc,
      errorMessage ?? this.errorMessage,
      _updateFunc,
    );
  }
}
