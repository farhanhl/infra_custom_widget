## 1.0.24

- fix default button border

## 1.0.23

- fix asset helper

## 1.0.22

- fix button padding, pin dialog keyboard type

## 1.0.21

- fix appbar toolbar heigh

## 1.0.20

- fix dropdown hint text, add bottomsheet widget

## 1.0.19

- add badge on button cards

## 1.0.18

- fix deprecated screeUtil size

## 1.0.17

- fix currency value controller

## 1.0.16

- fix TextFieldController text value with currency format

## 1.0.15

- fix fontFamily to applevel instead of using it in individual text

## 1.0.14

- fix ButtonCard icon size

## 1.0.13

- fix PopUp NewUpdateDialog background color

## 1.0.12

- fix datePicker onSurface Color

## 1.0.11

- fix timeline shape, textFields fontFamily

## 1.0.10

- add scaleFactor parameter to Text Widget

## 1.0.9

- fix indicator with index timeline widget color

## 1.0.8

- fix backgroundColor on some widgets

## 1.0.7

- add fontFamily for Texts Widget

## 1.0.6

- fix shimmer default BaseColor and HighlightColor.

## 1.0.5

- fix TimeLine overflow in vertical mode, fix Appbars background color when in darkmode and fix SearchDropdownController selectedItem disappear when Views are built.

## 1.0.4

- add ImagePicker.

## 1.0.3

- fix Dialogs Widget bug.

## 1.0.2

- fix bug and remove unused package.

## 1.0.1

- add more Widgets and Controllers.

## 1.0.0

- add Buttons and Appbars Widget.

## 0.0.2

- add Texts Widget.

## 0.0.1

- TODO: Describe initial release.
