import 'package:flutter/material.dart';
import 'package:infra_custom_widget/widgets/appbars.dart';
import 'package:infra_custom_widget/widgets/buttons.dart';
import 'package:infra_custom_widget/widgets/infra_custom_widget.dart';
import 'package:infra_custom_widget/widgets/texts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: Appbars.defaultAppbar(title: 'Home Page'),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const MyButton(),
              Texts.headline1('Headline 1'),
              Texts.body2('body 2'),
              Buttons.defaultButton(
                handler: () {},
                widget: Texts.button('Default Button'),
              ),
              Buttons.textButton(
                handler: () {},
                widget: Texts.button('Text Button'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
